# spring_boot_uploader_libs #

#### It contains some common codes that the following Spring Boot projects use, thru dependency ####
* [spring_boot_uploader_job](https://bitbucket.org/rajibpsarma/spring_boot_uploader_job)
* [spring_boot_uploader_cand](https://bitbucket.org/rajibpsarma/spring_boot_uploader_cand)

#### The following projects are part of a system that works together : ####
* [spring_boot_uploader_libs](https://bitbucket.org/rajibpsarma/spring_boot_uploader_libs)
* [spring_boot_uploader_job](https://bitbucket.org/rajibpsarma/spring_boot_uploader_job)
* [spring_boot_uploader_cand](https://bitbucket.org/rajibpsarma/spring_boot_uploader_cand)
* [uploader-ui](https://bitbucket.org/rajibpsarma/uploader-ui)