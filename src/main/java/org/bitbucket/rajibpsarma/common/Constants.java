package org.bitbucket.rajibpsarma.common;

public class Constants {
	public enum EntityType{
		JOB("J"), 
		CANDIDATE("C"), 
		JOB_APPLICATION("JA"), 
		JOB_APPLICATION_STAGES("AS"),
		CANDIDATE_NOTES("CN"),
		RETRIEVE_CANDIDATE("RC"),
		INTERVIEW_FEEDBACK("IF"),
		RETRIEVE_SEATHOLDERS("RS"),
		RETRIEVE_PROSPECTNOTES("RN"),
		RETRIEVE_INMAIL("RI"),
		RETRIEVE_STUBPROFILE("RP");
		
		private String type;
		
		private EntityType(String type){
			this.type = type;
		}
		
		public String getType() {
			return this.type;
		}
		public static EntityType forType(String type) {
			for(EntityType a : EntityType.values()) {
				if(a.type.equals(type))
					return a;
			}
			return null;
		}
		
		public String getCompleteType() {
			String completeType = this.type;
			
			switch (this.type) {
			case "J":
				completeType = "Job";
				break;
			case "C":
				completeType = "Candidate";
				break;
			case "JA":
				completeType = "Job Application";
				break;
			case "AS":
				completeType = "Job Application Stages";
				break;	
			case "CN":
				completeType = "Candidate Notes";
				break;
			case "RC":
				completeType = "Retrieve Candidate";
				break;
			case "IF":
				completeType = "Interview Feedback";
				break;
			case "RS":
				completeType = "Retrieve Seat Holders Information";
				break;		
			case "RN":
				completeType = "Retrieve Prospect Notes Information";
				break;	
			case "RI":
				completeType = "Retrieve In Mails Information";
				break;		
			case "RP":
				completeType = "Retrieve Stub Profile Information";
				break;					
			default:
				break;
			}
			
			return completeType;
		}		
		
		public boolean equals(EntityType type) {
			boolean result = false;
			
			if((type != null) && (type.getType().equals(this.type))) {
				result = true;
			}
			return result;
		}		
	}
}