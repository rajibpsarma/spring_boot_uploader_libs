package org.bitbucket.rajibpsarma.common.thread;

import java.util.List;

import org.bitbucket.rajibpsarma.common.Constants.EntityType;
import org.bitbucket.rajibpsarma.common.RecordVOI;
import org.json.simple.JSONArray;

public abstract class LinkedInBulkUploadThread implements Runnable {

	public abstract EntityType getEntityType();
	public abstract List<RecordVOI> getNextRecords(long lastEntityId);
	
	protected String domainName;
	protected EntityType type = null;
	
	@Override
	public void run() {
		type = getEntityType();
		String prefix = "Bulk Upload : " + type.getCompleteType() + " :";
		
		System.out.println(prefix + "*** Starting Bulk Upload of " + type.getCompleteType());
		System.out.println(prefix + " Fetching records ...");
		System.out.println(prefix + JSONArray.toJSONString(getNextRecords(1L)));
	}

}
