package org.bitbucket.rajibpsarma.common;

public interface RecordVOI {
	long getRecordID();
	boolean isValid();
	String getErrorMsg();
	void setRecordID(long recordID);
	void setErrorMsg(String errorMsg);
}
